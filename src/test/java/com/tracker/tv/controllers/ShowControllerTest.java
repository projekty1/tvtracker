package com.tracker.tv.controllers;

import com.tracker.tv.repositories.EpisodeRepository;
import com.tracker.tv.repositories.ShowRepository;
import com.tracker.tv.services.TVService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ShowControllerTest {

    @Mock
    ShowRepository showRepository;

    @Mock
    TVService tvService;

    @Mock
    EpisodeRepository episodeRepository;

    @InjectMocks
    ShowController showController;

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(showController)
                .build();
    }

    @Test
    void getAllShows() throws Exception{
        mockMvc.perform(get("/shows"))
                .andExpect(status().isOk())
                .andExpect(view().name("shows/myShows"))
                .andExpect(model().attributeExists("shows"));
        verify(showRepository, times(1)).findAll();
    }

    @Test
    void getAllEpisodesByShow() throws Exception{
        Long showId = 2L;
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);

        mockMvc.perform(get("/shows/"+showId+"/episodes"))
                .andExpect(status().isOk())
                .andExpect(view().name("episodes/episodeList"))
                .andExpect(model().attributeExists("episodes"));

        verify(episodeRepository, times(1)).findAllByShowId(argumentCaptor.capture());
        verifyNoMoreInteractions(episodeRepository);
        assertEquals(showId, argumentCaptor.getValue());
    }
}