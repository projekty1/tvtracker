package com.tracker.tv.controllers;

import com.tracker.tv.repositories.EpisodeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class EpisodeControllerTest {

    @Mock
    EpisodeRepository episodeRepository;

    @InjectMocks
    EpisodeController episodeController;

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(episodeController)
                .build();
    }

    @Test
    void getAllEpisodes() throws Exception{
        mockMvc.perform(get("/episodes"))
                .andExpect(status().isOk())
                .andExpect(view().name("episodes/episodeList"))
                .andExpect(model().attributeExists("episodes"));
        verify(episodeRepository, times(1)).findAll();

    }
}