INSERT INTO show (id, name) VALUES ('1', 'Homeland');
INSERT INTO show (id, name) VALUES ('2', 'Vikings');
INSERT INTO show (id, name) VALUES ('3', 'House of Cards');
INSERT INTO show (id, name) VALUES ('4', 'Peaky blinders');

INSERT INTO episode (id, episode_number, title, airdate, show_id) VALUES ('1', '1x1', 'Pilot', '2017-04-22', '2');