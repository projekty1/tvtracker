package com.tracker.tv.model;

public class FormSearch {

    private String showName;

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    @Override
    public String toString() {
        return "FormSearch{" +
                "showName='" + showName + '\'' +
                '}';
    }
}
