package com.tracker.tv.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Episode extends BaseEntity {

    private String title;
    private String episodeNumber;
    private LocalDate airdate;
    @ManyToOne(fetch = FetchType.LAZY)
    private Show show;
}
