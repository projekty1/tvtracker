package com.tracker.tv.repositories;

import com.tracker.tv.model.Episode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EpisodeRepository extends CrudRepository<Episode, Long> {
    List<Episode> findAllByShowId(Long showId);
}
