package com.tracker.tv.controllers;

import com.tracker.tv.model.FormSearch;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/index")
    public String getIndexPage(FormSearch formSearch){
        return "index";
    }
}
