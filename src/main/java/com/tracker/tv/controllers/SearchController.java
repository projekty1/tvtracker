package com.tracker.tv.controllers;

import com.tracker.tv.model.FormSearch;
import com.tracker.tv.services.TVService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Controller
public class SearchController implements WebMvcConfigurer {

    private TVService tvService;

    public SearchController(TVService tvService) {
        this.tvService = tvService;
    }

    @PostMapping("/search/shows")
    public String searchShowsByName(@ModelAttribute("formSearch") FormSearch formSearch, BindingResult errors, Model model){
        model.addAttribute("shows", tvService.searchShowByName(formSearch.getShowName()));
        return "search/showList";
    }

    @GetMapping("/search/shows")
    public String searchShowsByName(FormSearch formSearch){
        return "search/showList";
    }
}

