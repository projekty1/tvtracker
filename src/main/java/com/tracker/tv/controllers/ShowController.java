package com.tracker.tv.controllers;

import com.tracker.tv.repositories.EpisodeRepository;
import com.tracker.tv.repositories.ShowRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Controller
public class ShowController implements WebMvcConfigurer {

    private ShowRepository showRepository;
    private EpisodeRepository episodeRepository;

    public ShowController(ShowRepository showRepository, EpisodeRepository episodeRepository) {
        this.showRepository = showRepository;
        this.episodeRepository = episodeRepository;
    }

    @GetMapping("/shows")
    public String getAllShows(Model model){
        model.addAttribute("shows", showRepository.findAll());
        return "shows/myShows";
    }

    @GetMapping("/shows/{showId}/episodes")
    public String getAllEpisodesByShow(Model model, @PathVariable("showId") Long showId){
        model.addAttribute("episodes", episodeRepository.findAllByShowId(showId));
        return "episodes/episodeList";
    }
}

