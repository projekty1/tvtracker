package com.tracker.tv.controllers;

import com.tracker.tv.repositories.EpisodeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EpisodeController {

    private EpisodeRepository episodeRepository;

    public EpisodeController(EpisodeRepository episodeRepository) {
        this.episodeRepository = episodeRepository;
    }

    @GetMapping("/episodes")
    public String getAllEpisodes(Model model){
        model.addAttribute("episodes", episodeRepository.findAll());
        return "episodes/episodeList";
    }
}
