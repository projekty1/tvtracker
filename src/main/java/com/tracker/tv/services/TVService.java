package com.tracker.tv.services;

import com.tracker.tv.model.Show;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TVService {

    List<Show> searchShowByName(String name);

    Show getShowById(Long id);
}
