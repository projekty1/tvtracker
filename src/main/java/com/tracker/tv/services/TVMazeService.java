package com.tracker.tv.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tracker.tv.model.Show;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TVMazeService implements TVService {

    private final String TV_MAZE_URL = "http://api.tvmaze.com/";

    private RestTemplate restTemplate;

    private ObjectMapper objectMapper;

    public TVMazeService(RestTemplateBuilder restTemplateBuilder, ObjectMapper objectMapper) {
        this.restTemplate = restTemplateBuilder.build();
        this.objectMapper = objectMapper;
    }

    public List<Show> searchShowByName(String name){
        String requestUrl = TV_MAZE_URL+"search/shows?q=" +name;
        ArrayList<LinkedHashMap> showList = (ArrayList) restTemplate.getForObject(requestUrl, ArrayList.class).stream().map(o -> ((LinkedHashMap) o).get("show")).collect(Collectors.toList());
        List<Show> shows = new ArrayList<>();
        for(LinkedHashMap showObject : showList){
            shows.add(objectMapper.convertValue(showObject, Show.class));
        }
        return shows;
    }

    public Show getShowById(Long id){
        String requestUrl = TV_MAZE_URL+"shows/" +id;
        LinkedHashMap showObject = restTemplate.getForObject(requestUrl, LinkedHashMap.class);
        Show show = objectMapper.convertValue(showObject, Show.class);
        return show;
    }

}
